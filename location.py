import os

locations = ('Monkey Bars', 'Cafeteria', 'Parking Lot') #list of locations, using a tuple since this won't change
currentLocation = locations[0] #current location

def showLocations():
	os.system('clear')
	userchoice = None
	print "\n"
	for object in locations:
		print "	" + str(locations.index(object) + 1) + ") " + object
	print "\n"
	
	#check if it's a number
	try:
		userchoice = int(raw_input("Where do you want to go? "))
	except ValueError:
			showLocations()

	userchoice = userchoice - 1 #subtract one to get the choice in line with the tuple index
	if userchoice <= len(locations) and userchoice >= 0:
		setLocation(userchoice)
	else:
		showLocations()

def setLocation(location):
	currentLocation = locations[location]
	return True

def whereIsPlayer():
	return currentLocation