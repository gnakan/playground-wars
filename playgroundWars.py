import os
import location
from random import randint
from decimal import *

playerInventory = []
maxStorage = None #the number of units of items a player can store

currentStorage = None

itemList = ['Peanut Butter Sandwiches', 'Hot Cheetos']
market = []

#the amount of money a player has
playerCash = None

#runs once to set up the environment and random values for the market
def setup():
	global currentStorage
	global playerInventory
	global itemList
	global playerCash
	global maxStorage

	#set up the player's inventory
	for item in itemList:
		itemObject = {item : 0}
		playerInventory.append(itemObject)

	currentStorage = 0
	playerCash = 10
	maxStorage = 100

	#randomizePrices()
	start()

#randomizes prices for each of the items in the itemList
def randomizePrices():
	global market
	global itemList

	market = []

	for item in itemList:
		#randomPrice = "%0.2f" % (randint(int(1), int(100 * 100)) / 100.0) #generates a random price between 1 and 100 dollars
		randomPrice = "%0.2f" % Decimal(randint(int(1), int(10 * 10)) / 10) #generates a random price between 1 and 100 dollars

		marketObject = {item : randomPrice}
		market.append(marketObject)

	#start()

#starting point
def start():
	randomizePrices()

	randomBuy()

	os.system('clear')

	#the action a player can make
	action = ""

	buildStats()
	buildStash()
	
	
	print '\n'
	print "Johnny whispers to you:"
	print "     Psst, I got some stuff you might want."
	print ""

	#build the market list
	for item in market:
		choiceNumber = market.index(item) + 1
		for (i,t) in enumerate(item.items()):
			print "	" + str(choiceNumber) + ")" ,t[0].upper() + ": $" + str(t[1])

	print '\n'

	while action != "b" and action != "j":
		action = raw_input("Will you B>uy or J>et? ")
	if action == "b":
		buyItem()

#Allows player to buy an item
def buyItem():
	global playerCash
	global maxStorage
	
	try:
		playerChoice = int(raw_input("Which item you want kid? " ))
	except ValueError:
		buyItem()
	
	#look in the market and get the item name
	objectChoice = market[playerChoice - 1]
	itemName = objectChoice.keys()[0]
	itemCost = objectChoice.values()[0]

	purchase = int(raw_input("How many " + itemName + " do you want to buy? "))
	
	totalCost = float(itemCost) * float(purchase)
	

	

	#see if they have enough money
	if canPlayerAfford(totalCost) == False:
		print "Whoa tuna flavor....you don't have enough money to buy all that.."
		buyItem()


	playerCash = (playerCash - totalCost) #subtract the amount they spent

	updateInventory(itemName, purchase)

	#TODO see if they have enough space

	#now start over
	if totalCost > 0:
		raw_input("Thanks for the $" + str(totalCost) + " kid." +  '\n' +  "Press 'Enter' to continue")
	else:
		raw_input("Why are you wasting my time then? " +  '\n' "Press 'Enter' to continue")
	
	start()
		
#determines if a player can afford to make the purchase
def canPlayerAfford(purchase):
	remainingCash = playerCash - purchase
	if remainingCash > 0:
		return True
	else:
		return False

#asks to buy a random item from a player
def randomBuy():
	os.system('clear')
	marketItems = len(market) #number of items in market
	itemNumber = randint(0,marketItems - 1) #random item number in market

	randomMarketItem = market[itemNumber]
	itemName = randomMarketItem.keys()[0]
	itemPrice = randomMarketItem.values()[0]
	playerItemInventory =  getPlayerItemInventory(itemName)

	buildStats()

	if playerItemInventory > 0:
		print "Sabrina The Fiend says:"
		print "     Hey..my mom says I shouldn't eat " + itemName + ","
		print "     but I gotta have some!"
		print "     I'll buy all you got for $" + itemPrice + " a piece!" 
		print '\n'
		playerChoice = raw_input("Do you want to sell the " + str(playerItemInventory) + " " + itemName + " you have?" )
		if playerChoice == 'y':
			sellItem(itemName, playerItemInventory, itemPrice);
	else:
		print "Sabrina The Fiend says:"
		print "     Darn it! I have to get me some " + itemName + "."
		print "     I would have bought all you had for $" + itemPrice + " a piece! Maybe next time!"
		print '\n'
	raw_input("Press 'Enter' to continue")

def buildStash():
	print "              BACKPACK                   "
	print ""
	for object in playerInventory:
		for (i,t) in enumerate(object.items()):
			print t[0].upper() + ":" + str(t[1])
	print "*****************************************"

def buildStats():
	print "*****************************************"
	print "               STATS                     "
	print ""
	print "Cash: $" + str(playerCash)
	print "*****************************************"

def updateInventory(name, amount):
	global playerInventory

	for object in playerInventory:
		for key, value in object.items():
			if key == name:
				object[key] = object[key] + amount

def getPlayerItemInventory(name):
	global playerInventory
	for object in playerInventory:
		for key, value in object.items():
			if key == name:
				if value > 0:
					return value

	return False;

def sellItem(item, amount, price):
	global playerCash
	amount = int(amount)
	price = float(price)
	totalSale = (amount * price)
	playerCash = (playerCash + totalSale)

	#sell them all

	#turn it into a negative number and reuse the updateInventory function
	amount = -abs(amount)

	updateInventory(item, amount)

	return True

setup()